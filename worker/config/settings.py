from shared_utils.config.settings import SharedSettings


class WorkerSettings(SharedSettings):
    IPAPI: str


worker_settings = WorkerSettings()
