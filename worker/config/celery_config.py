from typing import List, Set
from pydantic import AmqpDsn
from shared_utils.config.settings import SharedSettings


class CelerySettings(SharedSettings):
    broker_url: str = None
    broker_host: str
    broker_user: str
    broker_password: str
    VHOST: str
    task_acks_late: bool = True
    worker_prefetch_multiplier: int = 1
    event_queue_ttl: int = 60
    worker_concurrency: int = 4
    task_serializer: str = 'json'
    accept_content: List[str] = ['application/json', 'json']
    result_backend: str = 'rpc'
    imports: Set[str] = None

    def init_broker_url(self, celery_imports: Set[str]):
        self.imports = celery_imports
        self.broker_url = AmqpDsn(
            url=f'amqp://{self.broker_user}:{self.broker_password}@{self.broker_host}/{self.VHOST}', scheme='amqp'
        )


celery_settings = CelerySettings()
