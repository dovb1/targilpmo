from abc import abstractmethod
from shared_utils.metaclasses.abstract_singleton import SingletonABCMeta


class BaseApi(metaclass=SingletonABCMeta):

    @abstractmethod
    def request(self):
        pass
