from logging import getLogger
from worker.handlers.base_api import BaseApi
import requests
import shared_utils.exceptions as exceptions
from requests.exceptions import HTTPError
from worker.config.settings import worker_settings
from shared_utils.consts import IP_SERVICE_FAIL_STATUS


class GeoLocationApi(BaseApi):
    def __init__(self):
        self.url = worker_settings.IPAPI
        self.logger = getLogger(__name__)

    def get_domain_server(self, domain: str):
        params = {'fields': 'query,status,message'}
        try:
            response = self.request(method="GET", uri=domain, params=params)
        except HTTPError as e:
            self.logger.error(f'Get http error with param {domain}. error: {e.__str__()}')
            raise exceptions.CannotGetDomainServer(e)
        except Exception as e:
            error_message = f'GeoLocation api {self.url} return error: {e.__str__()}'
            self.logger.error(error_message)
            raise exceptions.ServerUnknownError(error_message)
        if response.get("status") == IP_SERVICE_FAIL_STATUS:
            error_message = f'Geo Location api {worker_settings.IPAPI} returned fail because of {response.get("message")}'
            self.logger.error(error_message)
            raise exceptions.GeoLocationServiceError(error_message)
        return response.get("query")

    def get_domain_country(self, ip_address: str):
        params = {'fields': 'country,status,message'}
        try:
            response = self.request(method="GET", uri=ip_address, params=params)
        except HTTPError as e:
            self.logger.error(f'Get http error with param {ip_address}. error: {e.__str__()}')
            raise exceptions.CannotGetDomainCountry(e)
        except Exception as e:
            error_message = f'GeoLocation api {self.url} return error: {e.__str__()}'
            self.logger.error(error_message)
            raise exceptions.ServerUnknownError(error_message)
        if response.get("status") == IP_SERVICE_FAIL_STATUS:
            error_message = f'Geo Location api {worker_settings.IPAPI} returned fail because of {response.get("message")}'
            self.logger.error(error_message)
            raise exceptions.GeoLocationServiceError(error_message)
        return response.get("country")

    def get_domain_region(self, ip_address: str):
        params = {'fields': 'regionName,status,message'}
        try:
            response = self.request(method="GET", uri=ip_address, params=params)
        except HTTPError as e:
            self.logger.error(f'Get http error with param {ip_address}. error: {e.__str__()}')
            raise exceptions.CannotGetDomainRegion(e)
        except Exception as e:
            error_message = f'GeoLocation api {self.url} return error: {e.__str__()}'
            self.logger.error(error_message)
            raise exceptions.ServerUnknownError(error_message)
        if response.get("status") == IP_SERVICE_FAIL_STATUS:
            error_message = f'Geo Location api {worker_settings.IPAPI} returned fail because of {response.get("message")}'
            self.logger.error(error_message)
            raise exceptions.GeoLocationServiceError(error_message)
        return response.get("regionName")

    def request(self, method: str = 'GET', uri: str = '/', params: dict = None):
        response = requests.request(method=method, url=f'{self.url}{uri}', params=params if params is not None
        else {})
        response.raise_for_status()
        return response.json()
