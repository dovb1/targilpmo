import logging
import os
from pathlib import Path
from celery import Celery
from celery.signals import worker_init, setup_logging
from worker.config.celery_config import celery_settings
from shared_utils.db import GEOLOCATIONS_DATABASE_INSTANCE
from shared_utils.consts import WORKER_LOG_FILE

celery_settings.init_broker_url(['worker.tasks.geolocation'])
app = Celery("Targil")
app.config_from_object(celery_settings.dict())


@worker_init.connect
def worker_setup(*args, **kwargs):
    path = Path(os.path.realpath(__file__))
    filename = os.path.join(path.parent.absolute(), WORKER_LOG_FILE)
    logging.basicConfig(filename=filename, level=logging.INFO)
    GEOLOCATIONS_DATABASE_INSTANCE.connect()


@setup_logging.connect
def on_celery_setup_logging(**kwargs):
    pass
