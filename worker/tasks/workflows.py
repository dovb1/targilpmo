from celery.canvas import chain
from worker.tasks.geolocation import resolve_server, resolve_country, resolve_region


def get_domain_details(domain: str, workflow_id):
    workflow_tasks = [
        resolve_server.s(domain=domain, resource_name=domain, workflow_id=workflow_id),
        resolve_country.s(resource_name=domain, workflow_id=workflow_id),
        resolve_region.s(resource_name=domain, workflow_id=workflow_id)
    ]
    chain(workflow_tasks).apply_async()
