from logging import getLogger
from typing import Tuple
from worker.app import app
from worker.tasks.base_tasks import GeoLocationWorkflowTask
import worker.config.task_names as task_names
from worker.handlers.geolocation_handler import GeoLocationApi

logger = getLogger(__name__)


@app.task(base=GeoLocationWorkflowTask, workflow_name="tasks.geolocation.flow", name=task_names.RESOLVE_SERVER,
          task_description='resolving server', bind=True)
def resolve_server(self, domain: str, resource_name, workflow_id):
    logger.info(f'resolving domain {domain} for resource {resource_name} and workflow id {workflow_id}')
    ip_address = GeoLocationApi().get_domain_server(domain)
    return domain, ip_address


@app.task(base=GeoLocationWorkflowTask, workflow_name="tasks.geolocation.flow", name=task_names.RESOLVE_COUNTRY,
          task_description='resolving country', bind=True)
def resolve_country(self, tuple_obj: Tuple, resource_name, workflow_id):
    domain, ip_address = tuple_obj
    logger.info(f'resolving country for domain {domain}, for resource {resource_name} and workflow id {workflow_id}')
    country = GeoLocationApi().get_domain_country(ip_address)
    return domain, ip_address, country


@app.task(base=GeoLocationWorkflowTask, workflow_name="tasks.geolocation.flow", name=task_names.RESOLVE_REGION,
          task_description='resolving region', bind=True, is_last_task=True)
def resolve_region(self, tuple_obj: Tuple, resource_name, workflow_id):
    domain, ip_address, country = tuple_obj
    logger.info(f'resolving region for domain {domain} for resource {resource_name} and workflow id {workflow_id}')
    region = GeoLocationApi().get_domain_region(ip_address)
    return domain, ip_address, country, region
