from celery import Task as CeleryTask
from abc import ABC
from logging import getLogger
from shared_utils.db import GEOLOCATIONS_DATABASE_INSTANCE
from shared_utils.db.workflow_collection import Workflow
from celery import current_task
import shared_utils.exceptions as exceptions
from mongoengine.errors import MongoEngineException
from shared_utils.consts import SUCCESS, FAILED


class BaseWorkflow(CeleryTask, ABC):
    autoretry_for = (Exception,)
    max_retries = 3
    retry_backoff = True

    def __init__(self, Workflow=None):
        self.logger = getLogger(__name__)
        self.Workflow = Workflow
        self.is_last_task = self.is_last_task if hasattr(self, 'is_last_task') else False
        super().__init__()

    def __call__(self, *args, **kwargs):
        self.workflow_id = kwargs['workflow_id']
        self.resource_name = kwargs['resource_name']

        self.__set_workflow()
        return super().__call__(*args, **kwargs)

    def __set_workflow(self):
        workflow_obj = self.__get_create_workflow()
        workflow_obj.current_task_name = self.name
        workflow_obj.status = self.task_description
        workflow_obj.save()

    def __finish__workflow(self):
        workflow_obj = self.__get_create_workflow()
        workflow_obj.status = SUCCESS
        workflow_obj.save()

    def __failed_workflow(self, exception: Exception):
        workflow_obj = self.__get_create_workflow()
        workflow_obj.status = FAILED
        workflow_obj.exception_info = exception.__str__()
        workflow_obj.save()

    def __get_create_workflow(self):
        try:
            return GEOLOCATIONS_DATABASE_INSTANCE.get_workflow_info(self.workflow_id)
        except exceptions.WorkflowNotFound:
            try:
                workflow_obj = self.Workflow(
                    workflow_id=self.workflow_id,
                    resource_name=self.resource_name,
                    workflow_name=self.workflow_name,
                    current_task_name=current_task.name,
                    status="resolving server"
                )
                workflow_obj.save()
            except MongoEngineException as e:
                raise exceptions.CannotCreateWorkflowObject(e.__str__())
            return workflow_obj

    def __insert_new_domain(self, retval):
        domain, ip_address, country, region = retval
        try:
            geolocation = GEOLOCATIONS_DATABASE_INSTANCE.get_geolocation_domain_by_country(country, domain)
            GEOLOCATIONS_DATABASE_INSTANCE.update_geolocation(geolocation, ip_address, region, domain)
            self.logger.info(f'Updated domain {domain}, country {country} document')
        except exceptions.DocumentNotFound:
            GEOLOCATIONS_DATABASE_INSTANCE.create_new_geolocation(domain, ip_address, country, region)
            self.logger.info(f'Created new geolocation document with {domain}, {ip_address}, {country}, {region}')

    def __update_most_popular_domains(self, domain: str):
        try:
            res = GEOLOCATIONS_DATABASE_INSTANCE.get_most_popular_domain(domain)
            GEOLOCATIONS_DATABASE_INSTANCE.update_most_popular_domain(res)
            self.logger.info(f'Updated most popular domain {domain}')
        except exceptions.GetMostPopularDomainNotFound:
            GEOLOCATIONS_DATABASE_INSTANCE.create_most_popular_domain(domain)
            self.logger.info(f'Created new most popular domain {domain}')

    def __update_most_popular_servers(self, ip_address: str):
        try:
            res = GEOLOCATIONS_DATABASE_INSTANCE.get_most_popular_server(ip_address)
            GEOLOCATIONS_DATABASE_INSTANCE.update_most_popular_server(res)
            self.logger.info(f'Updated most popular server {ip_address}')
        except exceptions.GetMostPopularServerNotFound:
            GEOLOCATIONS_DATABASE_INSTANCE.create_most_popular_server(ip_address)
            self.logger.info(f'Created new most popular domain {ip_address}')

    def on_success(self, retval, task_id, args, kwargs):
        self.logger.info(f'finish workflow for domain {retval[0]}')
        if self.is_last_task:
            self.__finish__workflow()
            self.__insert_new_domain(retval)
            self.__update_most_popular_domains(retval[0])
            self.__update_most_popular_servers(retval[1])
        super().on_success(retval, task_id, args, kwargs)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        self.logger.error('failed get domain details')
        self.__failed_workflow(exc)
        super().on_failure(exc, task_id, args, kwargs, einfo)


class GeoLocationWorkflowTask(BaseWorkflow, ABC):
    def __init__(self):
        super().__init__(Workflow=Workflow)
