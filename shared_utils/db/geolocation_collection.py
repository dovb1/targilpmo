from mongoengine import Document, EmbeddedDocument, EmbeddedDocumentField
from mongoengine.fields import StringField, ListField


class Domain(EmbeddedDocument):
    name = StringField(required=True)
    servers = ListField(StringField(required=True))
    regions = ListField(StringField(required=True))


class GeoLocation(Document):
    country = StringField(required=True)
    domains = ListField(EmbeddedDocumentField(Domain), required=True)

    meta = {
        'collection': 'geolocations'
    }
