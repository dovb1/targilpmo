from mongoengine import Document
from mongoengine.fields import StringField


class Workflow(Document):
    workflow_id = StringField(required=True)
    resource_name = StringField(required=True)
    workflow_name = StringField(required=True)
    current_task_name = StringField(required=True)
    status = StringField(required=True)
    exception_info = StringField(required=False)

    meta = {
        'collection': 'workflows'
    }
