from logging import getLogger
from mongoengine import connect, disconnect
from mongoengine.queryset.visitor import Q
from shared_utils.db.workflow_collection import Workflow
import certifi
from shared_utils.db.geolocation_collection import GeoLocation, Domain
from shared_utils.db.most_pupolar_collection import PopularDomains, PopularServers, Server as PopularServer, \
    Domain as PopularDomain
import shared_utils.exceptions as exceptions
from shared_utils.config.settings import shared_settings
from mongoengine.errors import MongoEngineException
from mongoengine import QuerySet


class DataBase:
    logger = getLogger(__name__)

    @staticmethod
    def connect():
        try:
            connection = connect(host=shared_settings.MONGO_CONNECTION_STRING, tlsCAFile=certifi.where())
            db = connection.get_database(shared_settings.DB_NAME)
            db.list_collection_names()
        except MongoEngineException as e:
            error_message = f'Cannot connect to db {shared_settings.DB_NAME}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotConnectToDb(error_message)
        DataBase.logger.info(f'Successfully connect to db {shared_settings.DB_NAME}')

    @staticmethod
    def disconnect():
        disconnect(alias=shared_settings.DB_NAME)
        DataBase.logger.info(f'Successfully disconnected from db {shared_settings.DB_NAME}')

    @staticmethod
    def get_workflow_info(workflow_id: str):
        try:
            doc = Workflow.objects.get(workflow_id=workflow_id)
        except Workflow.DoesNotExist:
            error_message = f'Document with workflow id {workflow_id} was not found'
            DataBase.logger.error(error_message)
            raise exceptions.WorkflowNotFound(error_message)
        except MongoEngineException as e:
            error_message = f'Cannot get workflow document for workflow_id {workflow_id}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetWorkflowDocument(error_message)
        return doc

    @staticmethod
    def get_geolocation_domain_by_country(country: str, domain: str):
        try:
            geolocation = GeoLocation.objects.get(Q(country=country) & Q(domains__match={"name": domain}))
        except GeoLocation.DoesNotExist:
            geolocation = DataBase.get_geolocation_by_country(country)
        except MongoEngineException as e:
            error_message = f'Cannot get geolocation document by country {country} and domain {domain}. ' \
                            f'error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetDomainCountryDocument(error_message)
        return geolocation

    @staticmethod
    def get_geolocation_by_country(country: str):
        try:
            geolocation = GeoLocation.objects.get(Q(country=country))
        except GeoLocation.DoesNotExist:
            error_message = f'Geolocation domain by country document with params: country: {country} was not found'
            DataBase.logger.error(error_message)
            raise exceptions.GetGeoLocationByCountryNotFound(error_message)
        except MongoEngineException as e:
            error_message = f'Cannot get geolocation document by country {country}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetGeolocationByCountryDocument(error_message)
        return geolocation

    @staticmethod
    def update_geolocation(geolocation: GeoLocation, server: str, region: str, domain: str):
        is_domain_exist = False
        for domain_obj in geolocation.domains:
            if domain_obj.name == domain:
                is_domain_exist = True
                DataBase.update_current_domain(geolocation, domain_obj, server, region)
        if not is_domain_exist:
            DataBase.insert_new_domain(geolocation, server, region, domain)

    @staticmethod
    def insert_new_domain(geolocation: GeoLocation, server: str, region: str, domain: str):
        try:
            domain_obj = Domain(servers=[server], name=domain, regions=[region])
            geolocation.domains.append(domain_obj)
            geolocation.save()
        except MongoEngineException as e:
            error_message = f'Cannot append new domain {domain} for geolocation with country {geolocation.country}. ' \
                            f'error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotUpdateGeoLocation(error_message)

    @staticmethod
    def update_current_domain(geolocation: GeoLocation, domain_obj: Domain, server: str, region: str):
        is_changed = False
        if server not in domain_obj.servers:
            is_changed = True
            domain_obj.servers.append(server)
        if region not in domain_obj.regions:
            is_changed = True
            domain_obj.regions.append(region)
        if is_changed:
            geolocation.save()

    @staticmethod
    def get_geolocations_by_domain(domain: str):
        try:
            geolocations = GeoLocation.objects(Q(domains__match={"name": domain}))
            if len(geolocations) == 0:
                raise GeoLocation.DoesNotExist()
        except GeoLocation.DoesNotExist:
            error_message = f'Geolocation by domain {domain} document was not found'
            DataBase.logger.error(error_message)
            raise exceptions.GetGeoLocationByDomainNotFound(error_message)
        except MongoEngineException as e:
            error_message = f'Cannot get geolocation document by domain {domain}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetGeolocationByDomainDocument(error_message)
        return DataBase.__calculate_status__(geolocations, domain)

    @staticmethod
    def __calculate_status__(geolocations: QuerySet, wanted_domain: str):
        response = []
        for geolocation in geolocations:
            country = geolocation.country
            for domain in geolocation.domains:
                if domain.name == wanted_domain:
                    for region in domain.regions:
                        response.append(f'{country}/{region}')
        return response

    @staticmethod
    def get_domains_by_country(country: str):
        try:
            geolocations = GeoLocation.objects(Q(country=country) & Q(domains__match={"servers__ne": []}))
            if len(geolocations) == 0:
                raise GeoLocation.DoesNotExist()
        except GeoLocation.DoesNotExist:
            error_message = f'Domain by country {country} document was not found'
            DataBase.logger.error(error_message)
            raise exceptions.GetDomainsByCountryNotFound(error_message)
        except MongoEngineException as e:
            error_message = f'Cannot get geolocation domains document by country {country}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetGeolocationDomainsByCountryDocument(error_message)
        return DataBase.__get_domains_by_country__(geolocations)

    @staticmethod
    def __get_domains_by_country__(geolocations: QuerySet):
        domains = []
        for geolocation in geolocations:
            for domain in geolocation.domains:
                domains.append(domain.name)
        return domains

    @staticmethod
    def get_domains_by_server(server: str):
        try:
            geolocations = GeoLocation.objects(Q(domains__match={"servers__in": [server]}))
            if len(geolocations) == 0:
                raise GeoLocation.DoesNotExist()
        except GeoLocation.DoesNotExist:
            error_message = f'Domain by server {server} was not found'
            DataBase.logger.error(error_message)
            raise exceptions.GetDomainsByServerNotFound(error_message)
        except MongoEngineException as e:
            error_message = f'Cannot get geolocation domains by server {server}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetGeolocationDomainsByServerDocument(error_message)
        return DataBase.__get_domains_by_server(geolocations, server)

    @staticmethod
    def __get_domains_by_server(geolocations: QuerySet, server: str):
        domains = []
        for geolocation in geolocations:
            for domain in geolocation.domains:
                if server in domain.servers:
                    domains.append(domain.name)
        return domains

    @staticmethod
    def get_most_popular_domains(most_popular: int):
        try:
            most_popular_domains = PopularDomains.objects.order_by("-domain.count").limit(most_popular)
        except MongoEngineException as e:
            error_message = f'Cannot get most popular domains documents with limit {most_popular}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetMostPopularDomainsDocuments(error_message)
        return DataBase.__get_most_popular_domains_names(most_popular_domains)

    @staticmethod
    def __get_most_popular_domains_names(most_popular_domains: QuerySet):
        most_popular_domains_names = []
        for most_popular_domain in most_popular_domains:
            most_popular_domains_names.append(most_popular_domain.domain.name)
        return most_popular_domains_names

    @staticmethod
    def get_most_popular_servers(most_popular: int):
        try:
            most_popular_servers = PopularServers.objects.order_by("-server.count").limit(most_popular)
        except MongoEngineException as e:
            error_message = f'Cannot get most popular servers documents with limit {most_popular}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetMostPopularServersDocuments(error_message)
        return DataBase.__get_most_popular_servers_ips(most_popular_servers)

    @staticmethod
    def __get_most_popular_servers_ips(most_popular_servers: QuerySet):
        most_popular_servers_names = []
        for most_popular_server in most_popular_servers:
            most_popular_servers_names.append(most_popular_server.server.ip_address)
        return most_popular_servers_names

    @staticmethod
    def get_most_popular_domain(domain: str):
        try:
            most_popular_domain = PopularDomains.objects.get(Q(domain__name=domain))
        except PopularDomains.DoesNotExist:
            error_message = f'Most popular domain {domain} document was not found'
            DataBase.logger.error(error_message)
            raise exceptions.GetMostPopularDomainNotFound(error_message)
        except MongoEngineException as e:
            error_message = f'Cannot get most popular domain {domain}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetMostPopularDomain(error_message)
        return most_popular_domain

    @staticmethod
    def update_most_popular_domain(domain: PopularDomains):
        try:
            domain.domain.count += 1
            domain.save()
        except MongoEngineException as e:
            error_message = f'Cannot update domain {domain.domain.name} count. error {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotUpdatePopularDomain(error_message)

    @staticmethod
    def get_most_popular_server(server: str):
        try:
            most_popular_server = PopularServers.objects.get(Q(server__ip_address=server))
        except PopularServers.DoesNotExist:
            error_message = f'Most popular server {server} document was not found'
            DataBase.logger.error(error_message)
            raise exceptions.GetMostPopularServerNotFound(error_message)
        except MongoEngineException as e:
            error_message = f'Cannot get most popular server {server}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotGetMostPopularServerDocument(error_message)
        return most_popular_server

    @staticmethod
    def update_most_popular_server(server: PopularServers):
        try:
            server.server.count += 1
            server.save()
        except MongoEngineException as e:
            error_message = f'Cannot update server {server.server.name} count. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotUpdatePopularServer(error_message)

    @staticmethod
    def create_most_popular_domain(domain: str):
        try:
            domain_obj = PopularDomain(name=domain, count=1)
            most_popular = PopularDomains(domain=domain_obj)
            most_popular.save()
        except MongoEngineException as e:
            error_message = f'Cannot create popular domain {domain}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotCreatePopularDomain(error_message)

    @staticmethod
    def create_most_popular_server(server: str):
        try:
            server_obj = PopularServer(ip_address=server, count=1)
            most_popular = PopularServers(server=server_obj)
            most_popular.save()
        except MongoEngineException as e:
            error_message = f'Cannot create popular server {server}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotCreatePopularServer(error_message)

    @staticmethod
    def create_new_geolocation(domain: str, ip_address: str, country: str, region: str):
        try:
            domain_obj = Domain(servers=[ip_address], name=domain, regions=[region])
            geolocation_object = GeoLocation(country=country, domains=[domain_obj])
            geolocation_object.save()
        except MongoEngineException as e:
            error_message = f'Cannot create geolocation object with params: domain {domain}, server {ip_address}, ' \
                            f'country {country} and region {region}. error: {e.__str__()}'
            DataBase.logger.error(error_message)
            raise exceptions.CannotCreateGeolocationObject(error_message)


GEOLOCATIONS_DATABASE_INSTANCE = DataBase()
