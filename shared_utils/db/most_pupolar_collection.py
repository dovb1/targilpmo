from mongoengine import Document, EmbeddedDocument, EmbeddedDocumentField
from mongoengine.fields import StringField, ListField, IntField


class Domain(EmbeddedDocument):
    name = StringField(required=True)
    count = IntField(required=True)


class Server(EmbeddedDocument):
    ip_address = StringField(required=True)
    count = IntField(required=True)


class PopularDomains(Document):
    domain = EmbeddedDocumentField(Domain, required=True)

    meta = {
        'collection': 'popular_domains'
    }


class PopularServers(Document):
    server = EmbeddedDocumentField(Server, required=True)

    meta = {
        'collection': 'popular_servers'
    }
