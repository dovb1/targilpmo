from fastapi.requests import Request
from fastapi.responses import JSONResponse
from requests.exceptions import HTTPError


class CustomException(Exception):
    def __init__(self, error: str, status_code: int = 500):
        self.error = error
        self.status_code = status_code
        super().__init__(error)


# DB Section

class CannotConnectToDb(CustomException):
    def __init__(self, error: str):
        super().__init__(error)


class DocumentNotFound(CustomException):
    def __init__(self, error: str, status_code: int = 404):
        super().__init__(error, status_code)


class DocumentNotCreated(CustomException):
    def __init__(self, error: str, status_code: int = 400):
        super().__init__(error, status_code)


class DocumentNotUpdated(CustomException):
    def __init__(self, error: str, status_code: int = 400):
        super().__init__(error, status_code)


class DocumentCannotBeRetrieved(CustomException):
    def __init__(self, error: str, status_code: int = 400):
        super().__init__(error, status_code)


class WorkflowNotFound(DocumentNotFound):
    def __init__(self, error: str):
        super().__init__(error)


class GetGeoLocationByCountryNotFound(DocumentNotFound):
    def __init__(self, error: str):
        super().__init__(error)


class GetGeoLocationByDomainNotFound(DocumentNotFound):
    def __init__(self, error: str):
        super().__init__(error)


class GetDomainsByCountryNotFound(DocumentNotFound):
    def __init__(self, error: str):
        super().__init__(error)


class GetDomainsByServerNotFound(DocumentNotFound):
    def __init__(self, error: str):
        super().__init__(error)


class GetMostPopularDomainNotFound(DocumentNotFound):
    def __init__(self, error: str):
        super().__init__(error)


class GetMostPopularServerNotFound(DocumentNotFound):
    def __init__(self, error: str):
        super().__init__(error)


class CannotCreateGeolocationObject(DocumentNotCreated):
    def __init__(self, error: str):
        super().__init__(error)


class CannotCreateWorkflowObject(DocumentNotCreated):
    def __init__(self, error: str):
        super().__init__(error)


class CannotGetDomainCountryDocument(DocumentCannotBeRetrieved):
    def __init__(self, error: str):
        super().__init__(error)


class CannotGetGeolocationByCountryDocument(DocumentCannotBeRetrieved):
    def __init__(self, error: str):
        super().__init__(error)


class CannotGetMostPopularDomain(DocumentCannotBeRetrieved):
    def __init__(self, error: str):
        super().__init__(error)


class CannotGetMostPopularServerDocument(DocumentCannotBeRetrieved):
    def __init__(self, error: str):
        super().__init__(error)


class CannotGetWorkflowDocument(DocumentCannotBeRetrieved):
    def __init__(self, error: str):
        super().__init__(error)


class CannotGetGeolocationByDomainDocument(DocumentCannotBeRetrieved):
    def __init__(self, error: str):
        super().__init__(error)


class CannotGetGeolocationDomainsByCountryDocument(DocumentCannotBeRetrieved):
    def __int__(self, error: str):
        super().__init__(error)


class CannotGetGeolocationDomainsByServerDocument(DocumentCannotBeRetrieved):
    def __int__(self, error: str):
        super().__init__(error)


class CannotGetMostPopularDomainsDocuments(DocumentCannotBeRetrieved):
    def __int__(self, error: str):
        super().__init__(error)


class CannotGetMostPopularServersDocuments(DocumentCannotBeRetrieved):
    def __int__(self, error: str):
        super().__init__(error)


class CannotCreatePopularDomain(DocumentNotCreated):
    def __init__(self, error: str):
        super().__init__(error)


class CannotCreatePopularServer(DocumentNotCreated):
    def __init__(self, error: str):
        super().__init__(error)


class CannotUpdatePopularDomain(DocumentNotUpdated):
    def __init__(self, error: str):
        super().__init__(error)


class CannotUpdatePopularServer(DocumentNotUpdated):
    def __init__(self, error: str):
        super().__init__(error)


class CannotUpdateGeoLocation(DocumentNotUpdated):
    def __init__(self, error: str):
        super().__init__(error)


# HTTP Errors

class ServerUnknownError(CustomException):
    def __init__(self, error: str):
        super().__init__(error)


class GeoLocationServiceError(CustomException):
    def __init__(self, error: str):
        super().__init__(error)


class CannotGetDomainServer(HTTPError):
    pass


class CannotGetDomainCountry(HTTPError):
    pass


class CannotGetDomainRegion(HTTPError):
    pass


async def exception_handler(request: Request, e: CustomException):
    return JSONResponse(content=e.error, status_code=e.status_code)
