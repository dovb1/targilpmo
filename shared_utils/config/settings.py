from pydantic import BaseSettings


class SharedSettings(BaseSettings):
    DB_NAME: str
    MONGO_CONNECTION_STRING: str


shared_settings = SharedSettings()
