from logging import getLogger
import uuid
from worker.tasks.workflows import get_domain_details
from shared_utils.db import GEOLOCATIONS_DATABASE_INSTANCE
from shared_utils.consts import SUCCESS


class GeoLocation:
    logger = getLogger(__name__)

    @staticmethod
    def get_domain(domain: str):
        workflow_id = uuid.uuid4()
        GeoLocation.logger.info(f'getting data about domain {domain}')
        get_domain_details(domain, workflow_id)
        return workflow_id

    @staticmethod
    def get_geolocation_status(workflow_id: str):
        GeoLocation.logger.info(f'getting status of workflow id {workflow_id}')
        workflow_obj = GEOLOCATIONS_DATABASE_INSTANCE.get_workflow_info(workflow_id)
        if workflow_obj.status != SUCCESS:
            return {"status": workflow_obj.status, "error": workflow_obj.exception_info}
        else:
            return GEOLOCATIONS_DATABASE_INSTANCE.get_geolocations_by_domain(workflow_obj.resource_name)

    @staticmethod
    def get_domains_by_country(country: str):
        GeoLocation.logger.info(f'getting domains by country {country}')
        return GEOLOCATIONS_DATABASE_INSTANCE.get_domains_by_country(country)

    @staticmethod
    def get_domains_by_server(server: str):
        GeoLocation.logger.info(f'getting domains by server {server}')
        return GEOLOCATIONS_DATABASE_INSTANCE.get_domains_by_server(server)

    @staticmethod
    def get_most_popular_domains(most_popular: int):
        GeoLocation.logger.info(f'getting {most_popular} domains')
        return GEOLOCATIONS_DATABASE_INSTANCE.get_most_popular_domains(most_popular)

    @staticmethod
    def get_most_popular_servers(most_popular: int):
        GeoLocation.logger.info(f'getting {most_popular} servers')
        return GEOLOCATIONS_DATABASE_INSTANCE.get_most_popular_servers(most_popular)
    