import logging
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from api.routes.geolocation import geoLocationRouter
from shared_utils.db import GEOLOCATIONS_DATABASE_INSTANCE
import os
from pathlib import Path
from shared_utils.exceptions import CustomException, exception_handler
from shared_utils.consts import API_LOG_FILE


def get_app():
    app = FastAPI()

    app.include_router(geoLocationRouter, prefix="/geolocation", tags=["GeoLocations"])

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"]
    )

    @app.on_event('startup')
    async def startup_events():
        path = Path(os.path.realpath(__file__))
        filename = os.path.join(path.parent.absolute(), API_LOG_FILE)
        logging.basicConfig(filename=filename, level=logging.INFO)
        GEOLOCATIONS_DATABASE_INSTANCE.connect()

    @app.on_event('shutdown')
    async def shutdown_event():
        GEOLOCATIONS_DATABASE_INSTANCE.disconnect()

    app.add_exception_handler(CustomException, exception_handler)

    return app
