from logging import getLogger
from fastapi import APIRouter, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from api.models.geolocation import GeoLocation

geoLocationRouter = APIRouter()
logger = getLogger(__name__)


@geoLocationRouter.get("/")
def get_geolocation(domain: str):
    workflow_id = GeoLocation.get_domain(domain)
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={"workflow_id": jsonable_encoder(workflow_id)}
    )


@geoLocationRouter.get("/status")
def get_geolocation_request_status(workflow_id: str):
    response = GeoLocation.get_geolocation_status(workflow_id)
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder(response)
    )


@geoLocationRouter.get("/country")
def get_domains_by_country(country: str):
    response = GeoLocation.get_domains_by_country(country)
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder(response)
    )


@geoLocationRouter.get("/server")
def get_domains_by_server(server: str):
    response = GeoLocation.get_domains_by_server(server)
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder(response)
    )


@geoLocationRouter.get("/most_popular_domains")
def get_most_popular_domains(most_popular_domains: int):
    response = GeoLocation.get_most_popular_domains(most_popular_domains)
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder(response)
    )


@geoLocationRouter.get("/most_popular_servers")
def get_most_popular_servers(most_popular_servers: int):
    response = GeoLocation.get_most_popular_servers(most_popular_servers)
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder(response)
    )
